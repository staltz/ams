<div style="text-align:center;zoom:100%">

```mermaid
graph TD

  App-->usLens
  usLens-->USThermo
  App-->euLens
  euLens-->EUThermo

  classDef blue fill:#97cbfc,stroke:#333,stroke-width:1px;
  classDef yellow fill:#fce497,stroke:#333,stroke-width:1px;

  class App,USThermo,EUThermo blue;
  class euLens,usLens yellow;

  linkStyle 0 stroke:#000,stroke-width:1px;
  linkStyle 1 stroke:#000,stroke-width:1px;
  linkStyle 2 stroke:#000,stroke-width:1px;
  linkStyle 3 stroke:#000,stroke-width:1px;
```

</div>